#!/usr/bin/env python

from distutils.core import setup

setup(name='handystuff',
  version='0.0.1',
  description='Handy Stuff',
  author='Anybody Anybody',
  author_email='anybody@example.com',
  url='https://www.example.com',
  license="MIT",
  scripts=['bin/teststring.py'],
  packages=['handystuff']
)
